package com.zuitt.example;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;

public class S2A2 {
    public static void main(String[] args){
        int[] firstFivePrime = new int[5];

        firstFivePrime[0] = 2;
        firstFivePrime[1] = 3;
        firstFivePrime[2] = 5;
        firstFivePrime[3] = 7;
        firstFivePrime[4] = 11;

        System.out.println("The first prime number is: " + firstFivePrime[0]);

        ArrayList<String> friends = new ArrayList<String>(Arrays.asList());
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        System.out.println("My friends are: " + friends);

        HashMap<String, Integer> products = new HashMap<String, Integer>();
        products.put("toothpaste", 15);
        products.put("toothbrush", 20);
        products.put("soap", 12);
        System.out.println("Our current inventory consist of : " + products);
    }
}
